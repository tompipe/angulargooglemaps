﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularGoogleMaps.Models
{
    public enum Format
    {
        Csv = 0,
        Json,
        Xml,
        CsvWithSearch
    };
}
