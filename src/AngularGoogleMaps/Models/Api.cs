﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularGoogleMaps.Models
{
    public class Api
    {
        public string GoogleApiKey { get; set; }         

        public CoordinateSystem CoordinateSystem { get; set; }

        public Search Search { get; set; }

    }

    public class Search
    {
        public string Status { get; set; }

        public Limit Limit { get; set; }
    }

    public class Limit
    {
        public string Country { get; set; }
    }
}
